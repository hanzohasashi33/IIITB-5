# IIITB-5
## CPP_miniProject

CPP mini project submission.<br>
Contributors:<br>
IMT2019063 - Prasanna <br>
IMT2019059 - Nidhish <br>
IMT2019075 - Samaksh<br>
IMT2019077 - Satwik <br>


## Steps to run the project:

<ul>
    <li>git clone project</li>
    <li>cd src/</li>
    <li>g++ *.cpp -o output</li>
    <li>./output <filename></li>
</ul>