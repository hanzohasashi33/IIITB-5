//Header file for TextFile class

#include <string>
#include <fstream>
#include <sstream>
#include "DocumentStatistics.h"
#include "file.h"

using namespace std;

//TextFile class inherited from the class File that stores information particularly for text Files
//(Inheritence)

class TextFile : public File
{
    private:

        //(Composition)
        DocumentStatistics DocStats;    //Composition of class DocumentStatistics - TextFile "has" DocumentStatistics
    
    public:

        TextFile(string fileName);  //Constructor
        TextFile(const TextFile& tf2);  //Copy Constructor
        ~TextFile();    //Destructor

        //(Polymorphism)
        //Read and Write Functions specific to this TextFile class (overrided from the abstract functions in File class)
        void Read();
        void Write(string FileName);

        //Getter Function
        DocumentStatistics getDocumentStatistics();

};