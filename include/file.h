# ifndef FILE_H
# define FILE_H
# include <iostream>
# include <string>
# include <cstring>
# include <fstream>
# include <string>
# include <climits>
# include <vector>



// NAME: R Prasannavenkatesh
// ROLL: IMT2019063



// base class File
// all the other classes like ImageFile,CSVFile,TxtFile inherit from this.
class File 
{
    protected:
        std::string fileName;           // protected variable for file Name

    public:
        File(std::string fileName);    // main constructor
        File(const File& file);        // copy constructor
        virtual ~File();               // virtual destructor


        virtual void Read();                             // virtual method to read and computate values based on a file.
        virtual void Write(std::string fileName);        // virtual method to write to a file with the specified filename.
};




# endif 