# ifndef IMAGEFILE_H
# define IMAGEFILE_H
# include <iostream>
# include <string>
# include <cstring>
# include <string>
# include <climits>
# include <vector>
# include "image.h"
# include "file.h"


class ImageFile : public File
{
    private:
        std::string fileName;
        int rows,columns;                        // number of rows and columns
        std :: string comment;                   // comments of the file
        std :: string imageFormat = "P3";        // image format - p3 for ppm/ppma files
        int maxColorValue;                       // maximum color value of the constituent image pixels
        std :: vector<int> colorArr;
        Image RGBimage = Image(-1,-1,"a","a",-1,colorArr);    // image with RGB format - original image
        Image BRGimage = Image(-1,-1,"a","a",-1,colorArr);    // image with BRG format - changed image


    public:
        ImageFile(std::string fileName);
        

        void Read();                            // overriden method read
        void Write(std::string fileName);       // overriden method write

        std::string getImageFormat();           // getter to get image format
        int getRows();                          // getter to get number of rows
        int getColumns();                       // getter to get number of columns
        int getMaxColorValue();                 // getter to grt maximum color value
        Image getImage();                       // getter to get original image in RGB format
        Image getBRGImage();                    // getter to get image in BRG format
        Image getRGBImage();                    // getter to get orignal format image
};





# endif 