#include<iostream>

using namespace std;

//Document Statistics class that stores the statistics for a particular document

class DocumentStatistics
{
    private:

        int number_of_lines; //Variable to keep track of number of lines
        int number_of_characters; //Variable to keep track of number of characters
        int number_of_spaces; //Variable to keep track of number of spaces
        int number_of_words; //Variable to keep track of number of words
        int number_of_bytes; //Variable to keep track of number of bytes
    
    public:

        DocumentStatistics();   //Constructor
        DocumentStatistics(const DocumentStatistics& DS2);  //Copy Constructor
        ~DocumentStatistics();  //Destructor

        //Getter Functions
        int getLineCount();
        int getCharCount();
        int getSpaceCount();
        int getWordCount();
        int getBytesCount();

        //Setter Functions
        void setLineCount(int LineCount);
        void setCharCount(int CharCount);
        void setSpaceCount(int SpaceCount);
        void setWordCount(int WordCount);
        void setBytesCount(int BytesCount);

        //Friend function to overload the "<<" for DocumentStatistics class
        friend ostream &operator<<(ostream &output, const DocumentStatistics &DS2);
};