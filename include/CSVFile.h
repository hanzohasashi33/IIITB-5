#include "file.h"
#include<sstream>

using namespace std;

class CSVFile : public File
{
    private:

        vector<vector<string> > data;
        vector<vector<string> > transpose;

    public:

        CSVFile(string fileName);
        void Read();
        void Write(string FileName);

};
