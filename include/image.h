# ifndef IMAGE_H
# define IMAGE_H
# include <iostream>
# include <string>
# include <cstring>
# include <string>
# include <climits>
# include <vector>


// NAME: R Prasannavenkatesh
// ROLL: IMT2019063




class Image 
{
    private:
        int rows,columns;                        // number of rows and columns
        std :: string name;                      // name of the file
        std :: string imageFormat = "P3";        // image format - p3 for ppm/ppma files
        int maxColorValue;                       // maximum color value of the constituent image pixels
        std :: vector<int> colorArr;

    public:
        Image(int rows,int cols,std::string name,std::string imgForm,int maxColVal,std::vector<int> colorArr);         // constructor for Image object 
        Image(const Image &img);                                                             // copy constructor for Image object
        

        // getters
        std::string getImageFormat();         // getter for imageFormat
        int getRows();                        // getter to get number of rows
        int getColumns();                     // getter to get number of columns
        int getMaxColorValue();               // getter to get maximum color peak/values
        std::vector<int> getColorArray();     // color array.
        
        
        Image changeFormat();                 // method to change format from RGB to BRG

};




# endif