# include <iostream>
# include <string>
# include <vector>
# include <cmath>
# include <fstream>
# include <limits>

# include "../include/file.h"
# include "../include/image.h"
# include "../include/ImageFile.h"
# include "../include/Face.h"
# include "../include/Triangle.h"
# include "../include/Vertex.h"
# include "../include/CSVFile.h"
// # include "../include/DocumentStatistics.h"
# include "../include/TextFile.h"

# include "plyFile.cpp"


using namespace std;



// function to find extension of a filename
std::string extension(std::string fileName)
{
  //store the position of last '.' in the file name
  int position = fileName.find_last_of(".");
  
  //store the characters after the '.' from the file_name string
  std::string result = fileName.substr(position+1);
  return result;
}







int main(int argc, char** argv)
{
    // std :: cout << argv[1] << std :: endl;
    // std :: cout << argv[2] << std :: endl;
    // std :: cout << extension(argv[1]) << std::endl;

    std::string fileExtension = extension(argv[1]);    // get extension of the command line input file. 

    
    // if extension = ppm change the format of the given ppm file fron RGB to BRG 
    if(fileExtension == "ppm")
    {
        std::cout << "performing for ppm files" << std::endl;
        ImageFile ppmFile = ImageFile(argv[1]);
	    ppmFile.Read();
	    ppmFile.Write("brgImage.ppm");
        std::cout << "program performed successfully" << std::endl;
    }

    // if extension = csv Store the data from the file as a table/matrix, including the row and column field names, if any, and transpose it.
    else if(fileExtension == "csv")
    {
        std::cout << "performing for csv files" << std::endl;
        CSVFile csvfile = CSVFile(argv[1]);
        csvfile.Read();
        csvfile.Write("outputcheck.csv");
        std::cout << "program performed successfully" << std::endl;
    }


    // if extension = txt Generate the document statistics, as provided in gedit text editor application. Write the document statistics as a header and the current file as body, and generate a new .txt file
    else if(fileExtension == "txt")
    {
        std::cout << "performing for txt file" << std::endl;
        TextFile* tf = new TextFile(argv[1]);
        File* f = tf;

        f->Read();
        f->Write("newFile.txt");

        std::cout << "program performed successfully" << std::endl;
    
    }


    // if extension = ply Compute the area of each face in the graphical object, and sort the faces by area.
    else if(fileExtension == "ply")
    {
        std::cout << "performing for ply files" << std::endl;
        plyFile plyfile = plyFile(argv[1]);
        plyfile.Read();
        plyfile.Write("output.ply");
        std::cout << "program performed successfully" << std::endl;
    }

    // if extension = DL Sort the vertices/nodes based on its degree, and rank it based on the descending order of degree.
    else if(fileExtension == "DL")
    {
        std::cout << "performing for ply files" << std::endl;
        std::cout << "program performed successfully" << std::endl;
    }
    

    // if the given extension does not match the given tasks then end the program and print debug reports 
    else 
    {
        std::cout << "This file format extension is not supported." << std::endl;
        std::cout << "Please try an extension that is supported." << std::endl;
    }
    

    return 0;
}