# include <iostream>
# include <string>
# include <cstring>
# include <fstream>
# include <string>
# include <climits>
# include <vector>
# include "../include/image.h"
# include "../include/file.h"
# include "../include/ImageFile.h"


// image file main constructor
ImageFile::ImageFile(std::string fileName) : File(fileName)
{
	this->fileName = fileName;    
}


// overriden method to Read an image(.ppm file)
void ImageFile::Read()
{
    std :: ifstream file(fileName);                                // creating a new fstream.
	std :: cout << "file peek: " << file.peek() << std :: endl;
	while (file.peek() == '#')	 
	{
		// while the line is a comment ignore line and move forward
		std :: getline(file,comment);
	}
	
  	std :: getline(file,imageFormat);               // get image format line from ppm file.
	std :: cout << imageFormat << std :: endl;           
	

	while(file.peek() == '#')
	{
		// comment check.
		std :: getline(file,comment);
	}

	// int rows,columns;
	file >> rows >> columns;               // get rows and columns from file
	std :: cout << rows << " " << columns << std :: endl;
	std :: getline(file,comment);
	// file >> comment;
	
	std :: cout << "file peek: " << file.peek() << std :: endl;
	while(file.peek() == '#')
	{
		// comment check
		std :: getline(file,comment);
	}
	

	// int maxColVal;
	file >> this->maxColorValue;           // get max color value from file.
	std :: cout << maxColorValue << std :: endl;
	std :: getline(file,comment);
	// file >> comment;
	while(file.peek() == '#')
	{
		// comment check
		std :: getline(file,comment);
	}

	int pixelDim;
	while(file >> pixelDim)
	{
		// get pixel values from image
		// std :: cout << pixelDim << std :: endl;
		colorArr.push_back(pixelDim);
	}

	while(file.peek() == '#')
	{
		std :: getline(file,comment);
	}
	
	std :: cout << colorArr.size() << std :: endl;

	this->RGBimage = Image(rows,columns,"a",imageFormat,maxColorValue,colorArr);   // create the original image in RGB format
	this->BRGimage = RGBimage.changeFormat();                                      // create a BRG format image from given RGB image and store it
}






// overriden method write to write BGR image to output file specified
void ImageFile::Write(std::string fileName)
{
    this->fileName = fileName;
    std :: fstream my_file;
	my_file.open(fileName, std::ios::out);
	if (!my_file) 
	{
		std :: cout << "File not created!";
	}
	else 
	{
		std :: cout << "File created successfully!";
		my_file << this->BRGimage.getImageFormat() << std::endl;   // write the image format to file
		// my_file << comment << std::endl;
		my_file << BRGimage.getRows() << " " << BRGimage.getColumns() << std::endl;  // write rows and columns to file
		my_file << BRGimage.getMaxColorValue() << std::endl;        // write max color value to file
		for(int i = 0;i < BRGimage.getColorArray().size();++i)      // write the color pixels in BGR format to output file
		{
			if(i%12 == 0 && i != 0)
			{
				my_file << std::endl;
			}
			my_file << BRGimage.getColorArray()[i] << " ";
		}

		my_file.close();
	}
}




Image ImageFile::getImage()
{
    return this->BRGimage;
}


Image ImageFile::getBRGImage()
{
	return this->BRGimage;
}


Image ImageFile::getRGBImage()
{
	return this->RGBimage;
}



std::string ImageFile::getImageFormat()
{
    return this->imageFormat;
}

int ImageFile::getColumns()
{
    return this->columns;
}

int ImageFile::getRows()
{
    return this->rows;
}

int ImageFile::getMaxColorValue()
{
    return this->maxColorValue;
}