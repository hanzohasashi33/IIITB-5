#include "../include/DocumentStatistics.h"

//Implementation for Document Statistics class

//Constructor
DocumentStatistics::DocumentStatistics():number_of_lines(0),number_of_characters(0),number_of_spaces(0),number_of_words(0),number_of_bytes(0){}

//Copy COnstructor
DocumentStatistics::DocumentStatistics(const DocumentStatistics& DS2)
{
    this->number_of_lines = DS2.number_of_lines;
    this->number_of_characters = DS2.number_of_characters;
    this->number_of_spaces = DS2.number_of_spaces;
    this->number_of_words = DS2.number_of_words;
    this->number_of_bytes = DS2.number_of_bytes;
}

//Destructor
DocumentStatistics::~DocumentStatistics(){}

//Getter Functions
int DocumentStatistics::getLineCount(){ return number_of_lines;}
int DocumentStatistics::getCharCount(){ return number_of_characters;}
int DocumentStatistics::getSpaceCount(){ return number_of_spaces;}
int DocumentStatistics::getWordCount(){ return number_of_words;}
int DocumentStatistics::getBytesCount(){ return number_of_bytes;}

//Setter Functions
void DocumentStatistics::setLineCount(int LineCount){ number_of_lines=LineCount;}
void DocumentStatistics::setCharCount(int CharCount){ number_of_characters=CharCount;}
void DocumentStatistics::setSpaceCount(int SpaceCount){ number_of_spaces=SpaceCount;}
void DocumentStatistics::setWordCount(int WordCount){ number_of_words=WordCount;}
void DocumentStatistics::setBytesCount(int BytesCount){ number_of_bytes=BytesCount;}

//"<<" Overloading function
ostream &operator<<(ostream &output, const DocumentStatistics &DS2)
{
    output << "Number Of Lines = " << DS2.number_of_lines << endl;
    output << "Number Of Words = " << DS2.number_of_words << endl;
    output << "Number Of Characters (with spaces) = " << DS2.number_of_characters << endl;
    output << "Number Of Characters (without spaces) = " << DS2.number_of_characters - DS2.number_of_spaces << endl;
    output << "Number Of Bytes  = " << DS2.number_of_bytes << endl << endl << endl;
    return output;
}