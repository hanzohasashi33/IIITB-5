# include <iostream>
# include <string>
# include <cstring>
# include <string>
# include <climits>
# include "../include/image.h"


// main constructor
Image :: Image(int rows,int cols,std::string name,std::string imgForm,int maxColVal,std::vector<int> colorArr)
{
    this->rows = rows;
    this->columns = cols;
    this->name = name;
    this->imageFormat = imgForm;
    this->maxColorValue = maxColVal;
    this->colorArr = colorArr;
}


// copy constructor
Image :: Image(const Image &img)
{
    this->rows = img.rows;
    this->columns = img.columns;
    this->name = img.name;
    this->imageFormat = img.imageFormat;
    this->maxColorValue = img.maxColorValue;
    this->colorArr = img.colorArr;
}


// method to get image format
std::string Image::getImageFormat()
{
    return this->imageFormat;
}


// method to get number of columns
int Image::getColumns()
{
    return this->columns;
}


// method to get number of rows
int Image::getRows()
{
    return this->rows;
}


// method to get max color values
int Image::getMaxColorValue()
{
    return this->maxColorValue;
}


// method to change format of image from RGB to BRG
// returns a new image with required format
Image Image :: changeFormat()
{
    std::vector<int> newVector;     // new color arr vector to hold the pixel values of the BRG format image
    for(int i = 0;i < this->colorArr.size();i += 3)
    {
        // std :: iter_swap(this->colorArr.begin()+i,this->colorArr.begin()+(i+1));
        // std :: iter_swap(this->colorArr.begin()+i,this->colorArr.begin()+(i+2));
        // int temp = this->colorArri[i];
        // this->colorArr[i] = this->colorArr[i+1];
        // this->colorArr[i+1] = temp;

        // temp = this->colorArr[i];
        // this->colorArr[i] = this->colorArr[i+2];
        // this->colorArr[i+2] = temp;
        newVector.push_back(this->colorArr[i+2]);
        newVector.push_back(this->colorArr[i]);
        newVector.push_back(this->colorArr[i+1]);
    }
    // this->colorArr = newVector;
    return Image(this->rows,this->columns,this->name,this->imageFormat,this->maxColorValue,newVector);
}


// get the color array of image
std::vector<int> Image::getColorArray()
{
    return this->colorArr;
}