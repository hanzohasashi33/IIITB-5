# include "../include/file.h"
#include "../include/CSVFile.h"


CSVFile::CSVFile(string fileName) : File(fileName)
{
  // constructor is inherited from the case class file
}


void CSVFile::Read()
{
  //reading

  // Using fstream we will open the file and store them in a vector which contains vector of strings
  ifstream fx;
  fx.open(fileName,ios::in);

  while(fx)  //Checking if we've reached eof
  {
    vector<string> line;
    string l;
    getline(fx,l);  //reading line by line
    stringstream s(l);
    string word;
    while(getline(s,word,','))  //dividing data by ',' using stringstream and storing them
    {
        line.push_back(word);
    }
    data.push_back(line);
  }
  fx.close();


  //tranposing
  vector<string> ll;
  // Creating an empty vector which contains vector of strings of required size
  for(int i=0;i<data.size()-1;i++)
  {
    ll.push_back("");
  }
  for(int i=0;i<data[0].size();i++)
  {
    transpose.push_back(ll);
  }
  // copying the values in tranposed manner from the read data
  for(int i=0;i<data.size();i++)
  {
    for(int j=0;j<data.at(i).size();j++)
    {
      transpose[j][i]=data[i][j];
    }
  }
}


void CSVFile::Write(string FileName)
{
  //Writing

  // Using fstream we will write the transposed data by adding commas and next line character into given file
  fstream fout;
  fout.open(FileName, ios::out);
  for(int i=0;i<transpose.size();i++)
  {
    for(int j=0;j<transpose.at(i).size()-1;j++)
    {
      fout<<transpose[i][j]<<",";
    }
    fout<<transpose[i][transpose.at(i).size()-1]<<"\n";
  }

}
