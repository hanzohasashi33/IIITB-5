# include <iostream>
# include <string>
# include <cstring>
# include <fstream>
# include <string>
# include <climits>
# include <vector>
# include "../include/file.h"



// main constructor
File::File(std::string fileName)
{
    this->fileName = fileName;
}


// copy constructor
File::File(const File&file)
{
    this->fileName = file.fileName;
}


// virtual destructor
File::~File()
{

}


// virtual method read to read and computate file values
void File::Read()
{

}


// virtual method write to write the file values
void File::Write(std::string fileName)
{

}