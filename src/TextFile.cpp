# include "../include/file.h"
#include "../include/TextFile.h"

//Implementation for TextFile class

//Constructor
TextFile::TextFile(string fileName): File(fileName) //Base Class Initializer
{
    DocStats = DocumentStatistics();    //Creating instace of DocumentStatistics class
}

//Copy Constructor
TextFile::TextFile(const TextFile& tf2):File(tf2.fileName)
{
    DocStats = tf2.DocStats;
}

//Destructor
TextFile::~TextFile(){}


//Read Function for TextFile
//It reads the contents of given text file, and extracts the statistics of the document
void TextFile::Read()
{

    int number_of_lines=0; //Variable to keep track of number of lines
    int number_of_characters=0; //Variable to keep track of number of characters
    int number_of_spaces=0; //Variable to keep track of number of spaces
    int number_of_words=0; //Variable to keep track of number of words
    int number_of_bytes=0; //Variable to keep track of number of bytes

    ifstream textFile(fileName);    //Opening the text file in 'read' mode
    
    //Checking if Text File is opened or not
    if(textFile.is_open())
    {
        string str; //Stores the whole line of the document one by one
        
        int flag =0; //flag to check if there is any content in the text file

        //Reads the document until it does not get to read the next line, i.e., END OF FILE
        while(getline(textFile,str))
        {
            flag =1; //flag set to 1 as there is some content in the document

            //Counting the number of characters and spaces in the given line
            int i;
            for(i=0;i<str.length();i++)
            {
                number_of_characters++;
                if(str[i] == ' ')
                {
                    number_of_spaces++;
                }
            }

            //Splitting the line string with space as a delimitter to check number of words
            //Note - Also in particular token, there can be special characters (which also acts as delimmiters to words). So maintaining flags
            //       to find words in that token also
            // For example :- "(word)(word)" - are 2 words, "!!word!!word!!!!word" are 3 words according to 'gedit'

            string word;
            istringstream iss(str);
            while (iss.good())
            {
                iss >> word;

                int prevCharType = 0;   //Flag that sets if while traversing the token characterwise, previous character was not special character
                int currCharType = 0;   //Flag that sets if while traversing the token characterwise, current character is not special character

                //Traversing token character by character
                for(i=0;i<word.length();i++)
                {
                    char currChar = word[i];//Storing the current character

                    //Checking if current character is special character
                    if(!((currChar <= 57  && currChar >= 48) || (currChar <= 90  && currChar >= 65) || (currChar <= 122  && currChar >= 97)))
                    {
                        currCharType = 0;
                    }
                    else
                    {
                        currCharType = 1;
                    }
                    
                    //A word is counted only when previous character was a special character and current character is not special character
                    if( currCharType == 1 && prevCharType == 0)
                    {
                        number_of_words++;
                    }
                    
                    //Setting value of previous character
                    prevCharType = currCharType;
                }
            }
            
            number_of_lines++;  //Incrementing count of line
            
            number_of_spaces++; //Incrementing space count also as gedit considers the end line character of all lines except last line as space
            number_of_characters++;
            
        }

        if(flag==1)
        {
            number_of_spaces--; //Decrementing the space count by 1 (As the endline character of last line was also claculated in the space count)
            number_of_characters--;
        }

        number_of_bytes = number_of_characters; //Setting the number of bytes to be same as the number of characters as document is in ASCII format

        textFile.close();   //Closing the text file


        //Setting all the statistics in Document statistics
        DocStats.setLineCount(number_of_lines);
        DocStats.setCharCount(number_of_characters);
        DocStats.setSpaceCount(number_of_spaces);
        DocStats.setWordCount(number_of_words);
        DocStats.setBytesCount(number_of_bytes);
    }

}

//Implementing the Write Function for text file
//It writes the document statistics of the file at start of file (Header Part)
//It then copy and pastes the content of original text file below it (Body Part)
void TextFile::Write(string fileName2)
{
    ofstream newFile(fileName2);    //Creating and Opening the text file (with name 'fileName2' in same folder) as 'write' mode
    ifstream textFile(fileName);    //Opening the original text file in 'read' mode

    //Checking if both the files are opened or not
    if(textFile.is_open() && newFile.is_open())
    {
        newFile << DocStats;    //Writing the Document Statistics at the Header Part of File (Note- the "<<" function in overloaded for the DocumentStatistics class)

        //Reading Content from original Text File and Writing to the new Text File line by line.
        string str;
        while(getline(textFile,str))
        {
            newFile << str << endl;
        }

        textFile.close(); //Closing the original text file 
        newFile.close();  //Closing the new text file
    }

}

//Getter Function
DocumentStatistics TextFile::getDocumentStatistics(){ return DocStats;}